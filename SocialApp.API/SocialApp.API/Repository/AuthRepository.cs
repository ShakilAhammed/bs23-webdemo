﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using SocialApp.API.Models;
using Microsoft.EntityFrameworkCore;

namespace SocialApp.API.Repository
{
    public class AuthRepository : IAuthRepository
    {
        private readonly AppDbContext _context;

        public AuthRepository(AppDbContext context)
        {
            _context = context;
        }
        public async Task<User> RegisterUser(User user, string passWord)
        {
           

            //create hash password
            CreatePassword(passWord, out var passwordSalt, out var passwordHash);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            //save user to database
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return user;
        }

        private void CreatePassword(string passWord, out byte[] passwordSalt, out byte[] passwordHash)
        {
           using (var hmac =new HMACSHA512())
           {
               passwordSalt = hmac.Key;
               passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(passWord));
           }
        }

        public async Task<User> LogIn(string username, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);
            if (user == null)
            {
                return null;
            }

            return VerifyPasswordHash(password, user.PasswordSalt, user.PasswordHash) ? user : null;
        }

        private bool VerifyPasswordHash(string password, byte[] userPasswordSalt, byte[] userPasswordHash)
        {
            using (var hmac = new HMACSHA512(userPasswordSalt))
            {
                var storedPassword = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < userPasswordHash.Length; i++)
                {
                    if (userPasswordHash[i] != storedPassword[i]) return false;
                }

                return true;
            }
        }

        public async Task<bool> IsIsUserExist(string username)
        {
            if (await _context.Users.FirstOrDefaultAsync(x => x.Username == username) != null) return true;
            return false;
        }
    }
}
