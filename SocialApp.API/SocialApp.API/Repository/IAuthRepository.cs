﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SocialApp.API.Models;

namespace SocialApp.API.Repository
{
    public interface IAuthRepository
    {
        Task<User> RegisterUser(User user, string passWord);
        Task<User> LogIn(string username, string password);
        Task<bool> IsIsUserExist(string username);
    }
}
