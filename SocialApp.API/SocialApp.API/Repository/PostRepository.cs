﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocialApp.API.Models;
using SocialApp.API.Models.DB;
using SocialApp.API.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialApp.API.Repository
{
    public class PostRepository : IPostRepository
    {
        private readonly AppDbContext _context;

        public PostRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<PostListDto> GetAll( ItemFilterQuery filterQuery)
        {
            var query = _context.Posts.Include(m=>m.Comments).AsQueryable();
            var totalPosts = _context.Posts.Count();
            if(!string.IsNullOrWhiteSpace(filterQuery.PostName))
            {
                query = query.Where(p => p.Title.Contains(filterQuery.PostName));
            }

            if(filterQuery.StartIndex > 0 && filterQuery.PageSize > 0)
            {
                query = query.Skip(filterQuery.StartIndex - 1).Take(filterQuery.PageSize);
            }
            var posts = await _context.Posts.AsQueryable().ToListAsync();
            return new PostListDto() { Posts = posts, TotalPost = totalPosts };
        }
    }
}
