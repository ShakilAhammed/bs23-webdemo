﻿using SocialApp.API.Models.DB;
using SocialApp.API.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialApp.API.Repository
{
    public interface IPostRepository
    {
        Task<PostListDto> GetAll(ItemFilterQuery filterQuery);
    }
}
