﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialApp.API
{
    public enum VoteDirection
    {
        Up = 1,
        Neutral = 0,
        Down = -1
    }
}
