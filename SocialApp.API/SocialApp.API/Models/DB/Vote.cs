using System;

namespace SocialApp.API.Models
{
    public class Vote {
        public long Id { get; set; }
        public long UserId { get; set; }
        public virtual User User { get; set; }
        public VoteDirection Direction { get; set; }
     
    }
}