﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Bogus;
using Newtonsoft.Json;
using SocialApp.API.Models.DB;

namespace SocialApp.API.Models
{
    public class Seed
    {
        private readonly AppDbContext _context;

        public Seed(AppDbContext context)
        {
            _context = context;
        }

        public void SeedUsers()
        {
            var usersString = System.IO.File.ReadAllText("Models/UserSeedData.json");
            var users = JsonConvert.DeserializeObject<List<User>>(usersString);
            foreach (var user in users)
            {
                CreatePassword("password",out var passwordSalt, out var passwordHash);
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.Username = user.Username.ToLower();
                _context.Users.Add(user);
            }

            _context.SaveChanges();
        }

        public  void SeedPostsAsync()
        {
            var user = _context.Users.FirstOrDefault(m=>m.Id == 1);
            var post = new Post();
            post.Title = "Post One";
            post.WasDeleted = false;
            post.Body = "Post Body";
            post.User = user;
            _context.Posts.Add(post);
            _context.SaveChanges();

            var comment = new Comment();
            comment.Body = "Comment 1";
            comment.WasDeleted = false;
            comment.Upvotes = 120;
            comment.Downvotes = 40;
            comment.User = user;
            comment.Post = post;
            var comment2 = new Comment();
            comment2.Body = "Comment 2";
            comment2.WasDeleted = false;
            comment2.Upvotes = 140;
            comment2.Downvotes = 30;
            comment2.User = user;
            comment2.Post = post;

            var comment3 = new Comment();
            comment3.Body = "Comment 3";
            comment3.WasDeleted = false;
            comment3.Upvotes = 160;
            comment3.Downvotes = 10;
            comment3.User = user;
            comment3.Post = post;

            _context.Comments.Add(comment);
            _context.SaveChanges();
            _context.Comments.Add(comment2);
            _context.SaveChanges();
            _context.Comments.Add(comment3);
            _context.SaveChanges();

            _context.SaveChanges();


        }

        private void CreatePassword(string passWord, out byte[] passwordSalt, out byte[] passwordHash)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(passWord));
            }
        }
    }
}
