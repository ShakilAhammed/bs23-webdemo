﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SocialApp.API.Models.DB
{
    public class Post
    {
        public long Id { get; set; }
        public int Upvotes { get; set; }
        public int Downvotes { get; set; }
        public string Title { get; set; }
        public string Body { get; set; } = "";
        public DateTime CreationDate { get; } = DateTime.UtcNow;
        public bool WasUpdated { get; set; }
        public bool WasDeleted { get; set; }
        public int CommentCount { get; set; }

        public long UserId { get; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
