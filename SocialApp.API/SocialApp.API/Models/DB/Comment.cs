﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SocialApp.API.Models.DB
{
    public class Comment
    {
        public long Id { get; set; }

        public int Upvotes { get; set; }
        public int Downvotes { get; set; }
        public int ParentId { get; set; }
        public string Body { get; set; }
        public DateTime CreationDate { get; } = DateTime.UtcNow;
        public bool WasUpdated { get; set; }
        public bool WasDeleted { get; set; }

        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public long PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }

    }
}
