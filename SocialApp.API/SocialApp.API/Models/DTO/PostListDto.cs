﻿using SocialApp.API.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialApp.API.Models.DTO
{
    public class PostListDto
    {
        public List<Post> Posts { get; set; }
        public long TotalPost { get; set; }
    }
}
