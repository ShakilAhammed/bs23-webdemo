﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialApp.API.Models.DTO
{
    public class ItemFilterQuery
    {
        public int PageSize { get; set; }
        public int StartIndex { get; set; }
        public string PostName { get; set; }
    }
}
