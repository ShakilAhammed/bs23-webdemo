﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialApp.API.Models.DTO
{
    public class UserRegitrationDTO
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [StringLength(8, ErrorMessage = "Password must be between 6 to 8 chars", MinimumLength = 6)]
        public string Password { get; set; }
    }
}
