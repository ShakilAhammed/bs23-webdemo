﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SocialApp.API.Models;
using SocialApp.API.Models.DTO;
using SocialApp.API.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace SocialApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _authRepository;
        private readonly IConfiguration _config;

        public AuthController(IAuthRepository authRepository,
                              IConfiguration config)
        {
            _config = config;
            _authRepository = authRepository;
        }
        
        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser(UserRegitrationDTO userRegitrationDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userNameToLower = userRegitrationDto.Username.ToLower();

            if (await _authRepository.IsIsUserExist(userNameToLower))
            {
                return BadRequest("Username already exist");
            }
            var userToBeCreated = new User()
            {
                Username = userRegitrationDto.Username
            };

            var createdUser = await _authRepository.RegisterUser(userToBeCreated, userRegitrationDto.Password);

            return StatusCode(201);

        }

        [AllowAnonymous]
        [HttpPost("login")] 
        public async Task<IActionResult> Login(UserLoginDTO userLoginDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userFromRepo = await _authRepository.LogIn(userLoginDTO.Username.ToLower(), userLoginDTO.Password);
            if (userFromRepo == null)
                return Unauthorized();
            var identityClaims = new[] {
                new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                new Claim(ClaimTypes.Name, userFromRepo.Username)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor {
                Subject = new ClaimsIdentity(identityClaims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = credentials
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return Ok(new{
                token = tokenHandler.WriteToken(token)  
            });
        }
    }
}