using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SocialApp.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TestController: ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> PData()
        {
            return Ok("Protected for Admin");
        }
    }
}