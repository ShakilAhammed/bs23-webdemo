﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SocialApp.API.Models;
using SocialApp.API.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SocialApp.API.Installer
{
    public class DIInstaller : IInstaller
    {
        public void InstallService(IServiceCollection service, IConfiguration configuration)
        {
            service.AddScoped<IAuthRepository, AuthRepository>();
            service.AddScoped<IAppRepository, AppRepository>();
            service.AddScoped<IPostRepository, PostRepository>();
            service.AddTransient<Seed>();
        }
    }
}
