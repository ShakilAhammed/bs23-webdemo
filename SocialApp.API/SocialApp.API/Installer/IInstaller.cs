﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SocialApp.API.Installer
{
    interface IInstaller
    {
        void InstallService(IServiceCollection service, IConfiguration configuration);
    }
}
