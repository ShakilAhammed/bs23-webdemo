﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SocialApp.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SocialApp.API.Installer
{
    public class DbInstaller : IInstaller
    {
        public void InstallService(IServiceCollection service, IConfiguration configuration)
        {
            service.AddDbContext<AppDbContext>(x => x.UseLazyLoadingProxies().UseSqlServer(configuration.GetConnectionString("DefaultConnectionString")));
        }
    }
}
