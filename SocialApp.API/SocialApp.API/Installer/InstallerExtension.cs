﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SocialApp.API.Installer
{
    public static class InstallerExtension
    {
        public static void InstallAllServices(this IServiceCollection service, IConfiguration configuration)
        {
            var services = typeof(Startup).Assembly.ExportedTypes.Where(x => typeof(IInstaller).IsAssignableFrom(x) && !x.IsAbstract && !x.IsInterface).Select(Activator.CreateInstance).Cast<IInstaller>()
                .ToList();
            services.ForEach(x=> x.InstallService(service, configuration));
        }
    }
}
