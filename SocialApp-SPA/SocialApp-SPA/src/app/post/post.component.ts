import { Comment } from './../_interfaces/comment';
import { PostResponseDto } from './../_interfaces/post_response_dto';
import { AuthService } from './../_services/auth.service';
import { PostResponse } from './../_interfaces/post_response';
import { Component, OnInit, NgModule } from '@angular/core';
import { AlertifyService } from '../_services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostService } from '../_services/post.service';
@Component({
  selector: 'app-post',
  templateUrl: './post.component.html'
})
export class PostComponent implements OnInit {
  postResponse: PostResponse;
  postResponseDtos: Array<PostResponseDto>;
  constructor(private alertify: AlertifyService, private router: ActivatedRoute, private postService: PostService) {
    this.postService.getPosts().subscribe((data: PostResponse) => {
      console.log(data);
      this.postResponse = data;
      this.mapInfo();
    }, error =>{
      this.alertify.error(error)
    })
  }

  mapInfo() {
    let postResponseDtoos: Array<PostResponseDto> = []; 
    var posts = this.postResponse.posts;
    for(var i = 0; i<posts.length; ++i) {
      var post = new PostResponseDto();
      post.postTitle = posts[i].title;
      post.username = posts[i].user.username;
      post.isPost = true;
      post.creationDate = posts[i].creationDate;
      post.commentInfo = posts[i].comments.length + " comments";
      postResponseDtoos.push(post);
      for(var j = 0; j < posts[i].comments.length; ++j) {
        var comment = new PostResponseDto();
        comment.postTitle = posts[i].comments[j].body;
        comment.username = posts[i].comments[j].user.username;
        comment.isPost = false;
        comment.creationDate = posts[i].comments[j].creationDate;
        comment.commentInfo = "△" + posts[i].comments[j].upvotes + ", ▽ " +  posts[i].comments[j].upvotes;
        postResponseDtoos.push(comment);
      }
    }

    this.postResponseDtos = postResponseDtoos;
  }
  ngOnInit() {
  }
}