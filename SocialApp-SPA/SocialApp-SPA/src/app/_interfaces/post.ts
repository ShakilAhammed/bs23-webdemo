import { Comment } from './comment';
import { User } from './user';
export interface Post {
    user: User;
    comments: Comment[];
    id: number;
    upvotes: number;
    downvotes: number;
    title: string;
    body: string;
    creationDate: Date;
    wasUpdated: boolean;
    wasDeleted: boolean;
    commentCount: number;
    userId: number;
}