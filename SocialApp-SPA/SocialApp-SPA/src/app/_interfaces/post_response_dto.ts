export class PostResponseDto {
    postTitle: string;
    username: string;
    creationDate: Date;
    commentInfo: string;
    isPost: boolean;
}