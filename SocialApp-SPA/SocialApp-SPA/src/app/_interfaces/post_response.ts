import { Post } from './post';

export interface PostResponse {
    posts: Post[];
    totalPost: number;
}