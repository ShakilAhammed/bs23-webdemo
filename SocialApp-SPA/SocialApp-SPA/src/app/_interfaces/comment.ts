import { User } from './user';

export interface Comment {
    user: User;
    id: number;
    upvotes: number;
    downvotes: number;
    parentId: number;
    body: string;
    creationDate: Date;
    wasUpdated: boolean;
    wasDeleted: boolean;
    userId: number;
    postId: number;
}