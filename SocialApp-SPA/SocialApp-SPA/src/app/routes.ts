
import { AuthGuard } from './_guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { Routes } from '@angular/router';
import { PostComponent } from './post/post.component';


export const appRoutes: Routes = [
    {path: "", component: HomeComponent},
    {
        path: "", 
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children: [
            {path: "postandcomments", component: PostComponent},
            // {path: "members", component: MemberListComponent, resolve: {users: MemberListResolver}},
            // {path: "messages", component: MessagesComponent},   
            // {path: "member/edit", component: MemberEditComponent, resolve: {user: MemberEditResolver}, canDeactivate: [PreventUnsavedChangesGuard]},   
            // {path: "members/:id", component: MemberDetailsComponent, resolve: {user: MemberDetailsResolver}},   
        ]
    },
    
    {path: "**", redirectTo: "", pathMatch: "full"}
]