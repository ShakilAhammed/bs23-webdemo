import { AlertifyService } from './../_services/alertify.service';
import { AuthService } from './../_services/auth.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Output() toggleRegister = new EventEmitter();
  user: any = {}
  constructor(private authService: AuthService, private alertify: AlertifyService) { }

  ngOnInit() {
  }

  register() {
    this.authService.register(this.user).subscribe(()=>{
      this.alertify.success("Registration Successful.")
    }, error => {this.alertify.error(error)})
  }

  cancel() {
    this.toggleRegister.emit()
  }
}
