import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PostResponse } from '../_interfaces/post_response';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';


const httpOtions = {
  headers: new HttpHeaders({
    "Authorization": "Bearer " + localStorage.getItem('token')
  }),
  params : new HttpParams()
  .set('PageSize', "1")
  .set('StartIndex', "1")
  .set('PostName', "")
};


const params = new HttpParams()
  .set('PageSize', "1")
  .set('StartIndex', "1")
  .set('PostName', "");

@Injectable({
  providedIn: 'root'
})
export class PostService {
  baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }
  getPosts(): Observable<any> {
    console.log(this.baseUrl + 'post');
    return this.http.get(this.baseUrl + 'post', httpOtions);
  }
}
