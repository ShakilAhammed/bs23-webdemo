import { JwtHelperService } from "@auth0/angular-jwt";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
jwtHelper = new JwtHelperService()
userName:any ;
userId: any;
baseUrl = environment.apiUrl;
constructor(private httpClient: HttpClient) {
  if (this.isLoggedin()) {
    this.setUserNameFromToken();
  }
 }

login(model: any) {
  return this.httpClient.post(this.baseUrl + 'auth/login', model).pipe(
    map((response: any) =>{
      let user = response;
      if (user) {
        localStorage.setItem('token', user.token);
        this.setUserNameFromToken();
      }
    }
  ))
}

register(user: any) {
  return this.httpClient.post(this.baseUrl + 'auth/register', user)
}

isLoggedin() {
  var token = localStorage.getItem("token");
  if (token) {
    return !this.jwtHelper.isTokenExpired(token)
  }
  return false;
}

setUserNameFromToken() {
  var token = localStorage.getItem("token");
  var decodedToken = this.jwtHelper.decodeToken(token)
  if (decodedToken) {
    this.userName = decodedToken.unique_name;
    this.userId = decodedToken.nameid;
  }

}

}
